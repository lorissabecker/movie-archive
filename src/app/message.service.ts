import { Injectable } from '@angular/core';
import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material';
 
@Injectable({
  providedIn: 'root',
})
export class MessageService {
  messages: string[] = [];

  constructor(public snackBar: MatSnackBar) {}
 
  add(message: string, action: string) {
    this.openSnackBar(message, "");
  }
 
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  clear() {
    this.messages = [];
  }
}