import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { Movie } from '../movie';
import { MovieService } from '../movie.service';
import { AddMoviesDialogComponent } from '../add-movies-dialog/add-movies-dialog.component';
import { SearchMoviesDialogComponent } from '../search-movies-dialog/search-movies-dialog.component';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
	movies: Movie[];

  constructor(
  	private dialog: MatDialog,
  	private movieService: MovieService) {}

  readAllMovies(): void {
  	this.movieService.readAllMovies()
  		.subscribe(movies => this.movies = movies);
  }

	deleteMovie(movie: Movie): void {
		this.movies = this.movies.filter(m => m !== movie);
		this.movieService.deleteMovie(movie).subscribe();
	}

  openAddDialog() {
	  const dialogConfig = new MatDialogConfig();
	  dialogConfig.autoFocus = true;
	  let dialogRef = this.dialog.open(AddMoviesDialogComponent, dialogConfig);
	  dialogRef.afterClosed().subscribe(result => {
	  	if(result) {
	  		this.movies.push(result);
	  	}
    });
	}

	openSearchDialog() {
		const dialogConfig = new MatDialogConfig();
	  dialogConfig.autoFocus = true;

	  let dialogRef = this.dialog.open(SearchMoviesDialogComponent, dialogConfig);
	  dialogRef.afterClosed().subscribe(result => {
	  	if(result) {
	  		this.movies = result;
	  	}
    });
	}

  ngOnInit() {
  	this.readAllMovies();
  }
}
