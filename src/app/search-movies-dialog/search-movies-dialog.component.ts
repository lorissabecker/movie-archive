import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MovieService } from '../movie.service';
import { Movie } from '../movie';

@Component({
  selector: 'app-search-movies-dialog',
  templateUrl: './search-movies-dialog.component.html',
  styleUrls: ['./search-movies-dialog.component.css']
})
export class SearchMoviesDialogComponent implements OnInit {
	movies: Movie[];
  constructor(private dialogRef: MatDialogRef<SearchMoviesDialogComponent>, private movieService: MovieService) { }

  ngOnInit() {
  }

  searchMovies(condition: number, value: string): void {
  	switch(condition) {
  		case 1: {
  			// Search by Title
				this.movieService.searchMovies(value)
					.subscribe(movie => {
					  this.movies = movie;
					  console.log(this.movies);
					  this.dialogRef.close(this.movies);
					});
  			break;
  		}
  		case 2: {
  			// Search by Genre
  			break;
  		}
  		case 3: {
  			// Search by Actor
  			break;
  		}
  		default: {
  			// No search filter
  			this.movieService.readAllMovies()
  				.subscribe(movie => {
  					this.movies = movie;
  					this.dialogRef.close(this.movies);
  				});
  		}
  	}
  }

  setSearchFilter(value: string): void {
  	console.log("setSearchFilter: " + value);
  	this.searchMovies(1, value);
  }

  close() {
  	this.dialogRef.close();
  }
}
