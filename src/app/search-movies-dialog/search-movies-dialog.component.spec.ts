import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchMoviesDialogComponent } from './search-movies-dialog.component';

describe('SearchMoviesDialogComponent', () => {
  let component: SearchMoviesDialogComponent;
  let fixture: ComponentFixture<SearchMoviesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchMoviesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchMoviesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
