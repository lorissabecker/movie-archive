import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MovieService } from '../movie.service';
import { Movie } from '../movie';

@Component({
  selector: 'add-movies-dialog',
  templateUrl: './add-movies-dialog.component.html',
  styleUrls: ['./add-movies-dialog.component.css']
})
export class AddMoviesDialogComponent implements OnInit {
	movies: Movie[];
  constructor(private dialogRef: MatDialogRef<AddMoviesDialogComponent>, private movieService: MovieService) { }

  ngOnInit() { 
  	this.movies=[];
  }

  createMovie(title: string, genre: number, actor1: string, actor2: string, synopsis: string): void {
	  title = title.trim();
	  actor1 = actor1.trim();
	  actor2 = actor2.trim();
	  synopsis = synopsis.trim();
	  var sGenre = this.movieService.determineMovieGenre(genre);
	  console.log(sGenre);
	  if (!title || !actor1 || !actor2) { return; }

	  this.movieService.createMovie({ title: title, genre: sGenre, actor1: actor1, actor2: actor2, synopsis: synopsis } as Movie)
	    .subscribe(movie => {
	      this.movies.push(movie);
	      this.dialogRef.close(this.movies[0]);
	    });
	}

  close() {
  	this.dialogRef.close();
  }
}
