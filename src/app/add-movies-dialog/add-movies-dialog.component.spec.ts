import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMoviesDialogComponent } from './add-movies-dialog.component';

describe('AddMoviesDialogComponent', () => {
  let component: AddMoviesDialogComponent;
  let fixture: ComponentFixture<AddMoviesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMoviesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMoviesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
