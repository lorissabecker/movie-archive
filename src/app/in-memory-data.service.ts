import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const movies = [
      { id: 0, title: 'Gone With the Wind', genre: 'Romance', actor1: 'Scarlett OHara', actor2: 'Rhett Butler', synopsis: 'This is a synopsis of Gone With the Wind.' },
			{ id: 1, title: 'Aladdin', genre: 'Animation', actor1: 'Robin Williams', actor2: 'Brad Kane', synopsis: 'This is a synopsis of Aladdin.' },
			{ id: 2, title: 'Baby Driver', genre: 'Action', actor1: 'Ansel Elgort', actor2: 'Lily James', synopsis: 'This is a synopsis of Baby Driver.' },
			{ id: 3, title: 'Mother!', genre: 'Horror', actor1: 'Jennifer Lawrence', actor2: 'Javier Bardem', synopsis: 'This is a synopsis of Mother!.' },
			{ id: 4, title: 'Finding Dory', genre: 'Animation', actor1: 'Ellen DeGeneres', actor2: 'Albert Brooks', synopsis: 'This is a synopsis of Finding Dory.' },
			{ id: 5, title: 'A Quiet Place', genre: 'Horror', actor1: 'John Krasinski', actor2: 'Emily Blunt', synopsis: 'This is a synopsis of A Quiet Place.' },
			{ id: 6, title: 'Eragon', genre: 'Fantasy', actor1: 'Ed Speleers', actor2: 'Sienna Guillory', synopsis: 'This is a synopsis of Eragon.' },
			{ id: 7, title: 'Inkheart', genre: 'Fantasy', actor1: 'Brendan Fraser', actor2: 'Eliza Bennett', synopsis: 'This is a synopsis of Inkheart.' },
			{ id: 8, title: 'Avatar', genre: 'Science Fiction', actor1: 'Zoe Saldana', actor2: 'Sam Worthington', synopsis: 'This is a synopsis of Avatar.' },
			{ id: 9, title: 'How to Train Your Dragon', genre: 'Animation', actor1: 'Jay Baruchel', actor2: 'America Ferrera', synopsis: 'This is a synopsis of How To Train Your Dragon.' }
    ];
    return {movies};
  }
}
