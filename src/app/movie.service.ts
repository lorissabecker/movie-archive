import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Movie } from './movie';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
	private moviesUrl = 'api/movies';
	searchCondition: number;

  constructor(
  			private messageService: MessageService,
  			private http: HttpClient) { }

  readAllMovies(): Observable<Movie[]> {
  	this.log('Got movies!', '');
  	return this.http.get<Movie[]>(this.moviesUrl)
  		.pipe(
  			tap(movies => this.log('Fetched movies', '')),
  			catchError(this.handleError('readAllMovies', []))
  		);
  }

  searchMovies(value: string): Observable<Movie[]> {
  	console.log(value);
  	if(!value.trim()) {
			// If no title is searched for, return all movies
			return this.http.get<Movie[]>(this.moviesUrl)
	  		.pipe(
	  			tap(movies => this.log('Fetched movies', '')),
	  			catchError(this.handleError('readAllMovies', []))
	  		);
		}
		// Otherwise, return a list of movies with that title
		return this.http.get<Movie[]>('${this.moviesUrl}/?title=${value}').pipe(
			tap(_ => this.log('Found movies matching "${value}"', '')),
			catchError(this.handleError('searchMovies', []))
		);
  }

  createMovie(movie: Movie): Observable<Movie> {
  	return this.http.post<Movie>(this.moviesUrl, movie).pipe(
  		tap((movie: Movie) => this.log('Added new movie', 'Undo'))
  	);
  }

  deleteMovie(movie: Movie | number): Observable<Movie> {
  	const id = typeof movie === 'number' ? movie : movie.id;
  	const url = '${this.moviesUrl}/${id}';

  	return this.http.delete<Movie>(url);
  }

  getMovie(id: number): Observable<Movie> {
  	const url = '${this.moviesUrl}/$id)';
  	return this.http.get<Movie>(url).pipe(
  		tap(_ => this.log('Fetched movie id=${id}','')),
  		catchError(this.handleError<Movie>('getMovie id=${id}'))
  	);
  }

  determineMovieGenre(value: number): string {
  	switch(value) {
  		case 0: {
  			return "Abusrdist";
  			break;
  		}
  		case 1: {
  			return "Action";
  			break;
  		}
  		case 2: {
  			return "Comedy";
  			break;
  		}
  		case 3: {
  			return "Crime";
  			break;
  		}
  		case 4: {
  			return "Drama";
  			break;
  		}
  		case 5: {
  			return "Fantasy";
  			break;
  		}
  		case 6: {
  			return "Historical";
  			break;
  		}
  		case 7: {
  			return "Horror";
  			break;
  		}
  		case 8: {
  			return "Mystery";
  			break;
  		}
  		case 9: {
  			return "Romance";
  			break;
  		}
  		case 10: {
  			return "Science Fiction";
  			break;
  		}
  		case 11: {
  			return "Thriller";
  			break;
  		}
  		case 12: {
  			return "Western";
  			break;
  		}
  		default: {
  			return "No Genre";
  			break;
  		}
  	}
  }

  private log(message: string, action: string) {
  	this.messageService.add(message, action);
  }

	 /**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T> (operation = 'operation', result?: T) {
	  return (error: any): Observable<T> => {
	 
	    // TODO: send the error to remote logging infrastructure
	    console.error(error); // log to console instead
	 
	    // TODO: better job of transforming error for user consumption
	    //this.log(`${operation} failed: ${error.message}`);
	 
	    // Let the app keep running by returning an empty result.
	    return of(result as T);
	  };
	}

}
