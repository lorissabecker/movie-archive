export class Movie {
	id: number;
	title: string;
	genre: string;
	actor1: string;
	actor2: string;
	synopsis: string;
}